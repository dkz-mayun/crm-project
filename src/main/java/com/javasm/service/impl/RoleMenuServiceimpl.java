package com.javasm.service.impl;

import com.javasm.domin.entity.RoleMenu;
import com.javasm.service.RoleMenuService;
import com.javasm.service.base.impl.BaseServiceimpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class RoleMenuServiceimpl  extends BaseServiceimpl<RoleMenu> implements RoleMenuService {
}
